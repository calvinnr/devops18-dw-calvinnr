# Stage 2
## [Week 1](https://github.com/calvinnr/devops18-dw-calvinnr/tree/a02244776ce76b8cf7e978ba7f20cc10b349999f/Stage%202/Week%201)
### [Day 2 & 3 (Cloud Computing & Manage Server w/ Backend)](https://github.com/calvinnr/devops18-dw-calvinnr/blob/310614d0d8bd3605a30dda262a4856609897a029/Stage%202/Week%201/Day%203/README.md)
Task 1 - Instalasi VM di IDCH, Instalasi MySQL, Konfigurasi Aplikasi Frontend & Backend agar Saling Terhubung
## [Week 2](https://github.com/calvinnr/devops18-dw-calvinnr/tree/a02244776ce76b8cf7e978ba7f20cc10b349999f/Stage%202/Week%201)
### [Day 2, 3 & 4 (Docker & CI/CD Jenkins & Gitlab)](https://github.com/calvinnr/devops18-dw-calvinnr/blob/main/Stage%202/Week%202/Day%202/README.md)
Task 1 - Instalasi Docker, Deploy Aplikasi, Reverse Proxy & SSL Certificate on Top Docker
Task 2 - Jalankan Jenkins on Top Docker, Pipeline Frontend & Backend, Reverse Proxy & SSL Certificate, Auto Trigger Build Pipeline
